export { AuthActions } from "./src/auth/auth.actions";
export { AuthSelector } from "./src/auth/auth.selector";
export { AuthService } from "./src/auth/auth.service";
export { AuthState } from "./src/auth/auth.state";

export { AuthGuard } from "./src/auth/guards/auth.guard";

export { PageActions } from "./src/page/page.actions";
export { Page } from "./src/page/page.model";
export { PageSelector } from "./src/page/page.selector";
export { PageService } from "./src/page/page.service";
export { PageState } from "./src/page/page.state";

export { PostActions } from "./src/post/post.actions";
export { Post } from "./src/post/post.model";
export { PostSelector } from "./src/post/post.selector";
export { PostService } from "./src/post/post.service";
export { PostState } from "./src/post/post.state";

export { MenuActions } from "./src/menu/menu.actions";
export { Menu } from "./src/menu/menu.model";
export { MenuSelector } from "./src/menu/menu.selector";
export { MenuService } from "./src/menu/menu.service";
export { MenuState } from "./src/menu/menu.state";

export { BannerActions } from "./src/banner/banner.actions";
export { Banner } from "./src/banner/banner.model";
export { BannerSelector } from "./src/banner/banner.selector";
export { BannerService } from "./src/banner/banner.service";
export { BannerState } from "./src/banner/banner.state";

export { generateHexID } from "./src/utils/utils"

export { BlogState } from "./src/state";
export { BlogModule } from "./src/module";

import { authReducer } from "./src/auth/auth.reducer";
import { pageReducer } from "./src/page/page.reducer";
import { postReducer } from "./src/post/post.reducer";
import { menuReducer } from "./src/menu/menu.reducer";
import { bannerReducer } from "./src/banner/banner.reducer";

export const BLOG_REDUCER = {
	auth: authReducer,
	page: pageReducer,
	post: postReducer,
	menu: menuReducer,
	banner: bannerReducer
}