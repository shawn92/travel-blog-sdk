const path = require("path");

module.exports = {
    entry: {
        app: "./index.ts"
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "travel-blog-sdk.js"
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loaders: ["ts-loader", "angular2-template-loader"],
                exclude: "/node_modules/"
            },
            {
				test: /\.html$/,
                use: "html-loader",
                exclude: "/node_modules/"
			}
        ]
    },
    resolve: {
        extensions: [".ts", ".js"]
    },
    devServer: {
        contentBase: path.resolve(__dirname, "dist"),
        port: 3000
    }    
}