import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import "rxjs/add/observable/of";
import "rxjs/add/observable/merge";

import "rxjs/add/operator/catch";
import "rxjs/add/operator/do";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/let";
import "rxjs/add/operator/map";
import "rxjs/add/operator/switchMap";

import { AuthActions } from "./auth/auth.actions";
import { PageActions } from "./page/page.actions";
import { PostActions } from "./post/post.actions";
import { MenuActions } from "./menu/menu.actions";
import { BannerActions } from "./banner/banner.actions"; 

import { AuthEffects } from "./auth/auth.effects";
import { PageEffects } from "./page/page.effects";
import { PostEffects } from "./post/post.effects";
import { MenuEffects } from "./menu/menu.effects";
import { BannerEffects } from "./banner/banner.effects";

import { AuthSelector } from "./auth/auth.selector";
import { PageSelector } from "./page/page.selector";
import { PostSelector } from "./post/post.selector";
import { MenuSelector } from "./menu/menu.selector";
import { BannerSelector } from "./banner/banner.selector";

import { AuthService } from "./auth/auth.service";
import { PageService } from "./page/page.service";
import { PostService } from "./post/post.service";
import { MenuService } from "./menu/menu.service"; 
import { BannerService } from "./banner/banner.service";

import { AuthGuard } from "./auth/guards/auth.guard";
import { AuthInterceptor } from "./auth/interceptors/auth.interceptor";

const ACTIONS = [
  AuthActions,
  PageActions,
  PostActions,
  MenuActions,
  BannerActions
];

const GUARDS = [
  AuthGuard
];

const EFFECTS = [
  AuthEffects,
  PageEffects, 
  PostEffects, 
  MenuEffects, 
  BannerEffects
];

const SELECTORS = [
  AuthSelector,
  PageSelector,
  PostSelector,
  MenuSelector,
  BannerSelector
];

const SERVICES = [
  AuthService,
  PageService,
  PostService,
  MenuService,
  BannerService
];

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    EffectsModule.forRoot([
      ...EFFECTS
    ])
  ],
  providers: [
    ...ACTIONS,
    ...GUARDS,
    ...SELECTORS,
    ...SERVICES,
    { 
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ]
})
export class BlogModule { }