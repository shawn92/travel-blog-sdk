import { AuthState } from "./auth/auth.state";
import { PageState } from "./page/page.state";
import { PostState } from "./post/post.state";
import { MenuState } from "./menu/menu.state";
import { BannerState } from "./banner/banner.state";

export interface BlogState {
    auth: AuthState;
    page: PageState;
    post: PostState;
    menu: MenuState;
    banner: BannerState;
}