import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http"
import { Observable } from "rxjs/Observable";

import { Post } from "./post.model";

const API_URL = "http://localhost:8080/api/";
const POST_ENDPOINT = "posts";

@Injectable()
export class PostService {

    constructor(private httpClient: HttpClient) { }

    loadPosts(tag?: string): Observable<Post[]> {
        return !!tag  
            ? this.httpClient.get(`${API_URL}${POST_ENDPOINT}?tag=${tag}`).map((res: any) => res.posts)
            : this.httpClient.get(`${API_URL}${POST_ENDPOINT}`).map((res: any) => res.posts);
    }

    getPost(slug: string): Observable<Post> {
        return this.httpClient.get(`${API_URL}${POST_ENDPOINT}/${slug}`).map((res: any) => res.post);
    }

    addPost(post: Post): Observable<Post> {
        return this.httpClient.post(`${API_URL}${POST_ENDPOINT}`, post).map((res: any) => res.post);
    }

    updatePost(slug: string, post: Post): Observable<Post> {
        return this.httpClient.patch(`${API_URL}${POST_ENDPOINT}/${slug}`, post).map((res: any) => res.post);
    }

    deletePost(slug: string): Observable<Post> {
        return this.httpClient.delete(`${API_URL}${POST_ENDPOINT}/${slug}`).map((res: any) => res.post);
    }
}