import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";
import { Actions, Effect } from "@ngrx/effects";
import { Observable } from "rxjs/Observable";

import { POST_ACTIONS, PostAction, PostActions } from "./post.actions";
import { PostService } from "./post.service";
import { EditPostPayload } from "./post.state";

@Injectable()
export class PostEffects {

    constructor(
        private actions$: Actions,
        private postActions: PostActions,
        private postService: PostService
    ) {}

    @Effect() loadPosts$: Observable<Action> = this.actions$
        .ofType(POST_ACTIONS.loadPosts)
        .switchMap((action: PostAction) => this.postService.loadPosts(action.payload)
            .map(posts => this.postActions.loadPostsSuccess(posts))
            .catch((err: Error) => Observable.of(this.postActions.loadPostsFail(err)))
        );

    @Effect() getPosts$: Observable<Action> = this.actions$
        .ofType(POST_ACTIONS.getPost)
        .switchMap((action: PostAction) => this.postService.getPost(action.payload)
            .map(post => this.postActions.getPostSuccess(post))
            .catch((err: Error) => Observable.of(this.postActions.getPostFail(err)))
        );

    @Effect() addPost$: Observable<Action> = this.actions$
        .ofType(POST_ACTIONS.addPost)
        .switchMap((action: PostAction) => this.postService.addPost(action.payload)
            .map(post => this.postActions.addPostSuccess(post))
            .catch((err: Error) => Observable.of(this.postActions.addPostFail(err)))
        );

    @Effect() updatePost$: Observable<Action> = this.actions$
        .ofType(POST_ACTIONS.savePost)
        .map((action: PostAction) => action.payload as EditPostPayload)
        .switchMap(payload => this.postService.updatePost(payload.slug, payload.post)
            .map(post => this.postActions.savePostSuccess(post))
            .catch((err: Error) => Observable.of(this.postActions.savePostFail(err)))
        );
  
    @Effect() deletePost$: Observable<Action> = this.actions$
        .ofType(POST_ACTIONS.deletePost)
        .map((action: PostAction) => action.payload)
        .switchMap(payload => this.postService.deletePost(payload.slug)
            .map(post => this.postActions.deletePostSuccess(post)) 
            .catch((err: Error) => Observable.of(this.postActions.deletePostFail(err)))
        );
}