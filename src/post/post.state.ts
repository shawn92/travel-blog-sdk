import { Post } from "./post.model";

export interface PostState {
    posts?: Post[];
    error?: Error;
    current: Post;
}

export interface EditPostPayload {
    slug: string;
    post: Post;
}