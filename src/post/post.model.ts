export interface Post {
    _id: string;
    title: string;
    slug: string;
    content: string;
    tags?: string[];
    author: string;
    comments?: Comment[];
    createdAt?: Date;
    updatedAt?: Date;
}