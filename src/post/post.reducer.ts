import { PostAction, POST_ACTIONS } from "./post.actions";
import { Post } from "./post.model";
import { PostState } from "./post.state";

export const INITIAL_STATE: PostState = {
    current: undefined
};

export function postReducer(state = INITIAL_STATE, action: PostAction) {
    switch (action.type) {
        case POST_ACTIONS.loadPostsSuccess: {
            return {
                ...state,
                posts: action.payload as Post[]
            };
        }
        case POST_ACTIONS.getPostSuccess: {
            return {
                ...state,
                current: action.payload as Post
            };
        }
        case POST_ACTIONS.addPostSuccess: {
            return {
                ...state,
                posts: [...state.posts, action.payload] as Post[]
            };
        }
        case POST_ACTIONS.savePostSuccess: {
            const index = state.posts.findIndex(post => post._id === action.payload._id)
            const posts = [
                ...state.posts.slice(0, index),
                action.payload,
                ...state.posts.slice(index + 1)
            ] as Post[];
            return {
                ...state,
                posts
            };
        }
        case POST_ACTIONS.deletePostSuccess: {
            const index = state.posts.findIndex(post => post._id === action.payload._id)
            const posts = [
                ...state.posts.slice(0, index),
                ...state.posts.slice(index + 1)
            ] as Post[];
            return {
                ...state,
                posts
            };
        }
        case POST_ACTIONS.loadPostsFail:
        case POST_ACTIONS.getPostFail:
        case POST_ACTIONS.addPostFail:
        case POST_ACTIONS.savePostFail:
        case POST_ACTIONS.deletePostFail: {
            return { 
                ...state,
                error: action.payload 
            };
        }   
        default:
            return state;
    }
};