import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";

import { Post } from "./post.model";
import { EditPostPayload } from "./post.state";

export interface PostAction extends Action {
    payload?: any;
}

export const POST_ACTIONS = {
    loadPosts: "[Post] Load Posts",
    loadPostsSuccess: "[Post] Load Posts Success",
    loadPostsFail: "[Post] Load Posts Fail",

    getPost: "[Post] Get Post",
    getPostSuccess: "[Post] Get Post Success",
    getPostFail: "[Post] Get Post Fail",

    addPost: "[Post] Add Post",
    addPostSuccess: "[Post] Add Post Success",
    addPostFail: "[Post] Add Post Fail",

    savePost: "[Post] Save Post",
    savePostSuccess: "[Post] Save Post Success",
    savePostFail: "[Post] Save Post Fail",

    deletePost: "[Post] Delete Post",
    deletePostSuccess: "[Post] Delete Post Success",
    deletePostFail: "[Post] Delete Post Fail"
};

@Injectable()
export class PostActions {
    loadPosts(tag?: string): PostAction {
        return {
            type: POST_ACTIONS.loadPosts,
            payload: !!tag ? tag : undefined
        };
    }

    loadPostsSuccess(posts: Post[]): PostAction {
        return {
            type: POST_ACTIONS.loadPostsSuccess,
            payload: posts
        };
    }

    loadPostsFail(error: Error): PostAction {
        return {
            type: POST_ACTIONS.loadPostsFail,
            payload: error
        };
    }

    getPost(slug: string): PostAction {
        return {
            type: POST_ACTIONS.getPost,
            payload: slug
        };
    }

    getPostSuccess(post: Post): PostAction {
        return {
            type: POST_ACTIONS.getPostSuccess,
            payload: post
        };
    }

    getPostFail(error: Error): PostAction {
        return {
            type: POST_ACTIONS.getPostFail,
            payload: error
        };
    }

    addPost(post: Post): PostAction {
        return {
            type: POST_ACTIONS.addPost,
            payload: post
        };
    }

    addPostSuccess(post: Post): PostAction {
        return {
            type: POST_ACTIONS.addPostSuccess,
            payload: post
        };
    }

    addPostFail(error: Error): PostAction {
        return {
            type: POST_ACTIONS.addPostFail,
            payload: error
        };
    }

    savePost(slug: string, post: Post): PostAction {
        return {
            type: POST_ACTIONS.savePost,
            payload: { slug, post } as EditPostPayload
        };
    }

    savePostSuccess(post: Post): PostAction {
        return {
            type: POST_ACTIONS.savePostSuccess,
            payload: post
        };
    }

    savePostFail(error: Error): PostAction {
        return {
            type: POST_ACTIONS.savePostFail,
            payload: error
        };
    }

    deletePost(post: Post): PostAction {
        return {
            type: POST_ACTIONS.deletePost,
            payload: post
        };
    }

    deletePostSuccess(post: Post): PostAction {
        return {
            type: POST_ACTIONS.deletePostSuccess,
            payload: post
        };
    }

    deletePostFail(error: Error): PostAction {
        return {
            type: POST_ACTIONS.deletePostFail,
            payload: error
        };
    }
}