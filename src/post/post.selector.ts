import { Injectable } from "@angular/core";

import { BlogState } from "./../state";
import { Post } from "./post.model";

@Injectable()
export class PostSelector {
    getPosts() {
        return (state: BlogState): Post[] => state.post.posts; 
    }

    getCurrent() {
        return (state: BlogState): Post => state.post.current;
    }
}