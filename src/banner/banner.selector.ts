import { Injectable } from "@angular/core";

import { BlogState } from "./../state";
import { Banner } from "./banner.model";

@Injectable()
export class BannerSelector {
    getBanners() {
        return (state: BlogState): {} => state.banner.groups; 
    }
}