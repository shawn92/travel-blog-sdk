import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http"
import { Observable } from "rxjs/Observable";

import { Banner } from "./banner.model";

const API_URL = "http://localhost:8080/api/";
const BANNER_ENDPOINT = "banners";

@Injectable()
export class BannerService {

    constructor(private httpClient: HttpClient) { }

    loadBanners(group?: string): Observable<{}> {
        return !!group ? this.httpClient.get(`${API_URL}${BANNER_ENDPOINT}/${group}`).map((res: any) => res.banners)
            : this.httpClient.get(`${API_URL}${BANNER_ENDPOINT}`).map((res: any) => res.banners);
    }

    addBanner(banner: Banner): Observable<Banner> {
        return this.httpClient.post(`${API_URL}${BANNER_ENDPOINT}`, banner).map((res: any) => res.banner);
    }

    updateBanner(banner: Banner): Observable<Banner> {
        return this.httpClient.patch(`${API_URL}${BANNER_ENDPOINT}/${banner._id}`, banner).map((res: any) => res.banner);
    }

    deleteBanner(id: string): Observable<Banner> {
        return this.httpClient.delete(`${API_URL}${BANNER_ENDPOINT}/${id}`).map((res: any) => res.banner);
    }
}