import { BannerAction, BANNER_ACTIONS } from "./banner.actions";
import { Banner } from "./banner.model";
import { BannerState } from "./banner.state";

export const INITIAL_STATE: BannerState = {
    groups: {}
};

export function bannerReducer(state = INITIAL_STATE, action: BannerAction) {
    switch (action.type) {
        case BANNER_ACTIONS.loadBannersSuccess: {
            return {
                ...state,
                groups: {
                    ...state.groups,
                    ...action.payload
                }
            };
        }
        case BANNER_ACTIONS.addBannerSuccess: {
            const group = [
                ...state.groups[`${action.payload.group}`],
                action.payload
            ];
            return {
                ...state,
                groups: {
                    ...state.groups,
                    [action.payload.group]: group
                }
            };
        }
        case BANNER_ACTIONS.saveBannerSuccess: {
            let group = state.groups[`${action.payload.group}`] as Banner[];
            const index = group.findIndex(banner => banner._id === action.payload._id);
            group = [
                ...group.slice(0, index),
                action.payload,
                ...group.slice(index + 1)
            ];
            return {
                ...state,
                groups: {
                    ...state.groups,
                    [action.payload.group]: group
                }
            };
        }
        case BANNER_ACTIONS.deleteBannerSuccess: {
            let group = state.groups[`${action.payload.group}`] as Banner[];
            const index = group.findIndex(banner => banner._id === action.payload._id);
            group = [
                ...group.slice(0, index),
                ...group.slice(index + 1)
            ];
            return {
                ...state,
                groups: {
                    ...state.groups,
                    [action.payload.group]: group
                }
            };
        }
        default:
            return state;
    }
};