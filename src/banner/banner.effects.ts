import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";
import { Actions, Effect } from "@ngrx/effects";
import { Observable } from "rxjs/Observable";

import { BANNER_ACTIONS, BannerAction, BannerActions } from "./banner.actions";
import { BannerService } from "./banner.service";

@Injectable()
export class BannerEffects {

    constructor(
        private actions$: Actions,
        private bannerActions: BannerActions,
        private bannerService: BannerService
    ) {}

    @Effect() loadBanners$: Observable<Action> = this.actions$
        .ofType(BANNER_ACTIONS.loadBanners)
        .switchMap((action: BannerAction) => this.bannerService.loadBanners(action.payload)
            .map(banners => this.bannerActions.loadBannersSuccess(banners))
            .catch((err: Error) => Observable.of(this.bannerActions.loadBannersFail(err)))
        );

    @Effect() addBanner$: Observable<Action> = this.actions$
        .ofType(BANNER_ACTIONS.addBanner)
        .switchMap((action: BannerAction) => this.bannerService.addBanner(action.payload)
            .map(banner => this.bannerActions.addBannerSuccess(banner))
            .catch((error: Error) => Observable.of(this.bannerActions.addBannerFail(error)))
        );

    @Effect() saveBanner$: Observable<Action> = this.actions$
        .ofType(BANNER_ACTIONS.saveBanner)
        .switchMap((action: BannerAction) => this.bannerService.updateBanner(action.payload)
            .map(banner => this.bannerActions.saveBannerSuccess(banner))
            .catch((error: Error) => Observable.of(this.bannerActions.saveBannerFail(error)))
        );

    @Effect() deleteBanner$: Observable<Action> = this.actions$
        .ofType(BANNER_ACTIONS.deleteBanner)
        .map((action: BannerAction) => action.payload)
        .switchMap(payload => this.bannerService.deleteBanner(payload._id)
            .map(banner => this.bannerActions.deleteBannerSuccess(banner))
            .catch((error: Error) => Observable.of(this.bannerActions.deleteBannerFail(error)))
        );
}