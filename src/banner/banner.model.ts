export interface Banner {
    _id: string;
    title: string;
    url: string;
    group: string;
    width: number;
    height: number;
}
