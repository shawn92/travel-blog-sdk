import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";

import { Banner } from "./banner.model";

export interface BannerAction extends Action {
    payload?: any;
}

export const BANNER_ACTIONS = {
    loadBanners: "[Banner] Load Banners",
    loadBannersSuccess: "[Banner] Load Banners Success",
    loadBannersFail: "[Banner] Load Banners Fail",

    addBanner: "[Banner] Add Banner",
    addBannerSuccess: "[Banner] Add Banner Success",
    addBannerFail: "[Banner] Add Banner Fail",

    saveBanner: "[Banner] Save Banner",
    saveBannerSuccess: "[Banner] Save Banner Success",
    saveBannerFail: "[Banner] Save Banner Fail",

    deleteBanner: "[Banner] Delete Banner",
    deleteBannerSuccess: "[Banner] Delete Banner Success",
    deleteBannerFail: "[Banner] Delete Banner Fail"
};

@Injectable()
export class BannerActions {
    loadBanners(group?: string): BannerAction {
        return {
            type: BANNER_ACTIONS.loadBanners,
            payload: group
        };
    }

    loadBannersSuccess(banners: {}): BannerAction {
        return {
            type: BANNER_ACTIONS.loadBannersSuccess,
            payload: banners
        };
    }

    loadBannersFail(error: Error): BannerAction {
        return {
            type: BANNER_ACTIONS.loadBannersFail,
            payload: error
        };
    }

    addBanner(banner: Banner): BannerAction {
        return {
            type: BANNER_ACTIONS.addBanner,
            payload: banner
        };
    }

    addBannerSuccess(banner: Banner): BannerAction {
        return {
            type: BANNER_ACTIONS.addBannerSuccess,
            payload: banner
        };
    }

    addBannerFail(error: Error): BannerAction {
        return {
            type: BANNER_ACTIONS.addBannerFail,
            payload: error
        };
    }

    saveBanner(banner: Banner): BannerAction {
        return {
            type: BANNER_ACTIONS.saveBanner,
            payload: banner
        };
    }

    saveBannerSuccess(banner: Banner): BannerAction {
        return {
            type: BANNER_ACTIONS.saveBannerSuccess,
            payload: banner
        };
    }

    saveBannerFail(error: Error): BannerAction {
        return {
            type: BANNER_ACTIONS.saveBannerFail,
            payload: error
        };
    }

    deleteBanner(banner: Banner): BannerAction {
        return {
            type: BANNER_ACTIONS.deleteBanner,
            payload: banner
        };
    }

    deleteBannerSuccess(banner: Banner): BannerAction {
        return {
            type: BANNER_ACTIONS.deleteBannerSuccess,
            payload: banner
        };
    }

    deleteBannerFail(error: Error): BannerAction {
        return {
            type: BANNER_ACTIONS.deleteBannerFail,
            payload: error
        };
    }
}