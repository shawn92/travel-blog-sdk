import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

import { Action } from "@ngrx/store";
import { Actions, Effect } from "@ngrx/effects";

import { AUTH_ACTIONS, AuthAction, AuthActions } from "./auth.actions";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthEffects {

    constructor(
        private actions$: Actions,
        private authActions: AuthActions,
        private authService: AuthService
    ) {}

    @Effect() login$: Observable<Action> = this.actions$
        .ofType(AUTH_ACTIONS.login)
        .switchMap((action: AuthAction) => this.authService.login(action.payload.username, action.payload.password)
            .map(data => {
                this.authService.assignToken(data.token);
                return this.authActions.loginSuccess(data.username, data.token);
            })
            .catch((err: Error) => Observable.of(this.authActions.loginFail(err)))
        );

    @Effect() logout$: Observable<Action> = this.actions$
        .ofType(AUTH_ACTIONS.logout)
        .switchMap((action: AuthAction) => this.authService.logout(action.payload.username)
            .map(res => {
                if (res.status === 200) {
                    this.authService.removeToken();
                    return this.authActions.logoutSuccess()
                }
                return undefined;
            })
            .catch((err: Error) => Observable.of(this.authActions.logoutFail(err)))
        );
}