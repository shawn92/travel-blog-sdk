import { Injectable } from "@angular/core";

import { BlogState } from "./../state";

@Injectable()
export class AuthSelector {
    getUsername() {
        return (state: BlogState): {} => state.auth.username; 
    }

    getToken() {
        return (state: BlogState): {} => state.auth.token; 
    }
}