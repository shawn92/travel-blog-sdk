import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaderResponse, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

import { AUTH_TOKEN, AUTH_EXPIRES, AUTH_HEADER } from "./auth.config";

const API_URL = "http://localhost:8080/api/";
const AUTH_ENDPOINT = "users";

@Injectable()
export class AuthService {

    constructor(private httpClient: HttpClient) { }

    login(username: string, password: string): Observable<{username: string, token: string}> {
        return this.httpClient.post(`${API_URL}${AUTH_ENDPOINT}/login`, { username, password }, {
            observe: "response"
        }).map((res: HttpResponse<any>) => {
            return {
                username: res.body.username, 
                token: res.headers.get(AUTH_HEADER)
            }; 
        });
    }

    logout(username: string): Observable<Response> {
        const token = JSON.parse(localStorage.getItem(AUTH_TOKEN)).token;
        return this.httpClient.request("delete", `${API_URL}${AUTH_ENDPOINT}/logout`, { 
            body: { username, token }
        }).map((res: any) => res);
    }

    assignToken(token: string) {
        localStorage.setItem(AUTH_TOKEN, JSON.stringify({
            token, 
            expiration: new Date(+ new Date() + AUTH_EXPIRES)
        }));
    }

    removeToken() {
        localStorage.removeItem(AUTH_TOKEN);
    }

    isAuthenticated(): boolean {
        return !!localStorage.getItem(AUTH_TOKEN);
    }
}