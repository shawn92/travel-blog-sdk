import { Injectable } from "@angular/core";
import { 
    HttpRequest, 
    HttpHandler, 
    HttpEvent, 
    HttpInterceptor, 
    HttpEventType
} from "@angular/common/http";
import { Observable } from "rxjs/Observable";

import { AUTH_HEADER, AUTH_TOKEN } from "./../auth.config";
import { AuthService } from "./../auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!!localStorage.getItem(AUTH_TOKEN)) {
            const token = JSON.parse(localStorage.getItem(AUTH_TOKEN)).token;
            const secureRequest = request.clone({
                setHeaders: {
                    [AUTH_HEADER]: token
                }
            });
            return next.handle(secureRequest);
        }
        return next.handle(request);
    }
}
