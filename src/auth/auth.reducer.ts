import { AuthState } from "./auth.state";
import { AuthAction, AUTH_ACTIONS } from "./auth.actions";

export const INITIAL_STATE: AuthState = {
    username: undefined,
    token: undefined
}

export function authReducer(state = INITIAL_STATE, action: AuthAction) {
    switch(action.type) {
        case AUTH_ACTIONS.loginSuccess: {
            return {
                ...state,
                username: action.payload.username,
                token: action.payload.token
            };
        }
        case AUTH_ACTIONS.logoutSuccess: {
            return {
                ...state,
                INITIAL_STATE
            }
        }
        default:
            return state;
    }
}