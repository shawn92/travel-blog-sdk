import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";
import { Action } from "@ngrx/store";

export interface AuthAction extends Action {
    payload?: any;
}

export const AUTH_ACTIONS = {
    login: "[Auth] Login",
    loginSuccess: "[Auth] Login Success",
    loginFail: "[Auth] Login Fail",

    logout: "[Auth] Logout",
    logoutSuccess: "[Auth] Logout Success",
    logoutFail: "[Auth] Logout Fail"
};

@Injectable()
export class AuthActions {
    login(username: string, password: string): AuthAction {
        return {
            type: AUTH_ACTIONS.login,
            payload: { username, password }
        };
    }

    loginSuccess(username: string, token: string): AuthAction {
        return {
            type: AUTH_ACTIONS.loginSuccess,
            payload: { username, token }
        };
    }

    loginFail(error: Error): AuthAction {
        return {
            type: AUTH_ACTIONS.loginFail,
            payload: error
        };
    }

    logout(username: string): AuthAction {
        return {
            type: AUTH_ACTIONS.logout,
            payload: username
        };
    }

    logoutSuccess(): AuthAction {
        return {
            type: AUTH_ACTIONS.logoutSuccess
        };
    }

    logoutFail(error: Error): AuthAction {
        return {
            type: AUTH_ACTIONS.logoutFail
        };
    }
}