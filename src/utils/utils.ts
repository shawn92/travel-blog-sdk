import * as crypto from "crypto";

export function generateHexID() {
    return crypto.randomBytes(12).toString("hex");
}