import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Action } from "@ngrx/store";
import { Actions, Effect } from "@ngrx/effects";

import { MENU_ACTIONS, MenuAction, MenuActions } from "./menu.actions";
import { MenuService } from "./menu.service";

@Injectable()
export class MenuEffects {

    constructor(
        private actions$: Actions,
        private menuActions: MenuActions,
        private menuService: MenuService
    ) {}

    @Effect() loadMenus$: Observable<Action> = this.actions$
        .ofType(MENU_ACTIONS.loadMenus)
        .switchMap((action: MenuAction) => this.menuService.loadMenus(action.payload)
            .map(menus => this.menuActions.loadMenusSuccess(menus))
            .catch((err: Error) => Observable.of(this.menuActions.loadMenusFail(err)))
        );

    @Effect() addMenu$: Observable<Action> = this.actions$
        .ofType(MENU_ACTIONS.addMenu)
        .switchMap((action: MenuAction) => this.menuService.addMenu(action.payload)
            .map(menu => this.menuActions.addMenuSuccess(menu))
            .catch((err: Error) => Observable.of(this.menuActions.addMenuFail(err)))
        );

    @Effect() saveMenu$: Observable<Action> = this.actions$
        .ofType(MENU_ACTIONS.saveMenu)
        .map((action: MenuAction) => action.payload)
        .switchMap(payload => this.menuService.updateMenu(payload.slug, payload.menu)
            .map(menu => this.menuActions.saveMenuSuccess(menu))
            .catch((err: Error) => Observable.of(this.menuActions.saveMenuFail(err)))
        );

    @Effect() deleteMenu$: Observable<Action> = this.actions$
        .ofType(MENU_ACTIONS.deleteMenu)
        .map((action: MenuAction) => action.payload)
        .switchMap(payload => this.menuService.deleteMenu(payload.slug)
            .map(menu => this.menuActions.deleteMenuSuccess(menu))
            .catch((err: Error) => Observable.of(this.menuActions.deleteMenuFail(err)))
        );
}