import { Page } from "./../page/page.model";

export interface Menu {
    _id: string;
    slug: string;
    pages?: Page[];
}