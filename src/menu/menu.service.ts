import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http"
import { Observable } from "rxjs/Observable";

import { Menu } from "./menu.model";

const API_URL = "http://localhost:8080/api/";
const BANNER_ENDPOINT = "menus";

@Injectable()
export class MenuService {

    constructor(private httpClient: HttpClient) { }

    loadMenus(slug?: string): Observable<{}> {
        return !!slug ? this.httpClient.get(`${API_URL}${BANNER_ENDPOINT}/${slug}`).map((res: any) => res.menus)
            : this.httpClient.get(`${API_URL}${BANNER_ENDPOINT}`).map((res: any) => res.menus);
    }

    addMenu(menu: Menu): Observable<Menu> {
        return this.httpClient.post(`${API_URL}${BANNER_ENDPOINT}`, menu).map((res: any) => res.menu);
    }

    updateMenu(slug: string, menu: Menu): Observable<Menu> {
        return this.httpClient.patch(`${API_URL}${BANNER_ENDPOINT}/${slug}`, menu).map((res: any) => res.menu);
    }

    deleteMenu(slug: string): Observable<Menu> {
        return this.httpClient.delete(`${API_URL}${BANNER_ENDPOINT}/${slug}`).map((res: any) => res.menu);
    }
}