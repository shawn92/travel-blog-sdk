import * as _ from "lodash";
import { Injectable } from "@angular/core";

import { BlogState } from "./../state";
import { Menu } from "./menu.model";

@Injectable()
export class MenuSelector {
    getMenus() {
        return (state: BlogState): Menu[] => _.toArray(state.menu.menus); 
    }
}