import * as _ from "lodash";

import { MenuAction, MENU_ACTIONS } from "./menu.actions";
import { Menu } from "./menu.model";
import { MenuState } from "./menu.state";

export const INITIAL_STATE: MenuState = {
    menus: {}
};

export function menuReducer(state = INITIAL_STATE, action: MenuAction) {
    switch (action.type) {
        case MENU_ACTIONS.loadMenusSuccess: {
            return {
                ...state,
                menus: {
                    ...state.menus,
                    ...action.payload
                }
            };
        }
        case MENU_ACTIONS.addMenuSuccess: {
            return {
                ...state,
                menus: {
                    ...state.menus,
                    [action.payload.slug]: action.payload
                }
            };
        }
        case MENU_ACTIONS.saveMenuSuccess: {
            const menus = _.omitBy(state.menus, menu => menu._id === action.payload._id);
            return {
                ...state,
                menus: {
                    ...menus,
                    [action.payload.slug]: action.payload
                }
            };
        }
        case MENU_ACTIONS.deleteMenuSuccess: {
            const menus = _.omitBy(state.menus, menu => menu._id === action.payload._id);
            return {
                ...state,
                menus: {
                    ...menus
                }
            };
        }
        default:
            return state;
    }
}