import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";

import { Menu } from "./menu.model";

export interface MenuAction extends Action {
    payload?: any;
}

export const MENU_ACTIONS = {
    loadMenus: "[Menu] Load Menus",
    loadMenusSuccess: "[Menu] Load Menus Success",
    loadMenusFail: "[Menu] Load Menus Fail",

    addMenu: "[Menu] Add Menu",
    addMenuSuccess: "[Menu] Add Menu Success",
    addMenuFail: "[Menu] Add Menu Fail",

    saveMenu: "[Menu] Save Menu",
    saveMenuSuccess: "[Menu] Save Menu Success",
    saveMenuFail: "[Menu] Save Menu Fail",

    deleteMenu: "[Menu] Delete Menu",
    deleteMenuSuccess: "[Menu] Delete Menu Success",
    deleteMenuFail: "[Menu] Delete Menu Fail"
};

@Injectable()
export class MenuActions {
    loadMenus(slug?: string): MenuAction {
        return {
            type: MENU_ACTIONS.loadMenus,
            payload: slug
        };
    }

    loadMenusSuccess(menus: {}): MenuAction {
        return {
            type: MENU_ACTIONS.loadMenusSuccess,
            payload: menus
        };
    }

    loadMenusFail(error: Error): MenuAction {
        return {
            type: MENU_ACTIONS.loadMenusFail,
            payload: error
        };
    }

    addMenu(menu: Menu): MenuAction {
        return {
            type: MENU_ACTIONS.addMenu,
            payload: menu
        };
    }

    addMenuSuccess(menu: Menu): MenuAction {
        return {
            type: MENU_ACTIONS.addMenuSuccess,
            payload: menu
        };
    }

    addMenuFail(error: Error): MenuAction {
        return {
            type: MENU_ACTIONS.addMenuFail,
            payload: error
        };
    }

    saveMenu(slug: string, menu: Menu): MenuAction {
        return {
            type: MENU_ACTIONS.saveMenu,
            payload: { slug, menu }
        };
    }

    saveMenuSuccess(menu: Menu): MenuAction {
        return {
            type: MENU_ACTIONS.saveMenuSuccess,
            payload: menu
        };
    }

    saveMenuFail(error: Error): MenuAction {
        return {
            type: MENU_ACTIONS.saveMenuFail,
            payload: error
        };
    }

    deleteMenu(menu: Menu): MenuAction {
        return {
            type: MENU_ACTIONS.deleteMenu,
            payload: menu
        };
    }

    deleteMenuSuccess(menu: Menu): MenuAction {
        return {
            type: MENU_ACTIONS.deleteMenuSuccess,
            payload: menu
        };
    }

    deleteMenuFail(error: Error): MenuAction {
        return {
            type: MENU_ACTIONS.deleteMenuFail,
            payload: error
        };
    }
}