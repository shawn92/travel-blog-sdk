import { Page } from "./page.model";

export interface PageState {
    pages?: Page[];
    current?: Page;
    error?: Error;
}

export interface EditPagePayload {
    slug: string;
    page: Page;
}