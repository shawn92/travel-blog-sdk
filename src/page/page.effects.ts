import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";
import { Actions, Effect } from "@ngrx/effects";
import { Observable } from "rxjs/Observable";

import { PAGE_ACTIONS, PageAction, PageActions } from "./page.actions";
import { Page } from "./page.model";
import { PageService } from "./page.service";
import { EditPagePayload } from "./page.state";

@Injectable()
export class PageEffects {

    constructor(
        private actions$: Actions,
        private pageActions: PageActions,
        private pageService: PageService
    ) {}

    @Effect() loadPages$: Observable<Action> = this.actions$
        .ofType(PAGE_ACTIONS.loadPages)
        .switchMap(() => this.pageService.loadPages()
            .map(pages => this.pageActions.loadPagesSuccess(pages))
            .catch((err: Error) => Observable.of(this.pageActions.loadPagesFail(err)))
        );

    @Effect() getPage$: Observable<Action> = this.actions$
        .ofType(PAGE_ACTIONS.getPage)
        .switchMap((action: PageAction) => this.pageService.getPage(action.payload)
            .map(page => this.pageActions.getPageSuccess(page))
            .catch((err: Error) => Observable.of(this.pageActions.getPageFail(err)))
        );

    @Effect() addPage$: Observable<Action> = this.actions$
        .ofType(PAGE_ACTIONS.addPage)
        .switchMap((action: PageAction) => this.pageService.addPage(action.payload)
            .map(page => this.pageActions.addPageSuccess(page))
            .catch((err: Error) => Observable.of(this.pageActions.addPageFail(err)))
        );

    @Effect() updatePage$: Observable<Action> = this.actions$
        .ofType(PAGE_ACTIONS.savePage)
        .map((action: PageAction) => action.payload as EditPagePayload)
        .switchMap(payload => this.pageService.updatePage(payload.slug, payload.page)
            .map(page => this.pageActions.savePageSuccess(page))
            .catch((err: Error) => Observable.of(this.pageActions.savePageFail(err)))
         );

    @Effect() deletePage$: Observable<Action> = this.actions$
        .ofType(PAGE_ACTIONS.deletePage)
        .map((action: PageAction) => action.payload)
        .switchMap(payload => this.pageService.deletePage(payload.slug)
            .map(page => this.pageActions.deletePageSuccess(page))
            .catch((err: Error) => Observable.of(this.pageActions.deletePageFail(err)))
        );
}