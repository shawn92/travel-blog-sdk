import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http"
import { Observable } from "rxjs/Observable";

import { Page } from "./page.model";

const API_URL = "http://localhost:8080/api/";
const PAGE_ENDPOINT = "pages";

@Injectable()
export class PageService {

    constructor(private httpClient: HttpClient) { }

    loadPages(): Observable<Page[]> {
        return this.httpClient.get(`${API_URL}${PAGE_ENDPOINT}`).map((res: any) => res.pages);
    }

    getPage(slug: string): Observable<Page> {
        return this.httpClient.get(`${API_URL}${PAGE_ENDPOINT}/${slug}`).map((res: any) => res.page);
    }

    addPage(page: Page): Observable<Page> {
        return this.httpClient.post(`${API_URL}${PAGE_ENDPOINT}`, page).map((res: any) => res.page);
    }

    updatePage(slug: string, page: Page): Observable<Page> {
        return this.httpClient.patch(`${API_URL}${PAGE_ENDPOINT}/${slug}`, page).map((res: any) => res.page);
    }

    deletePage(slug: string): Observable<Page> {
        return this.httpClient.delete(`${API_URL}${PAGE_ENDPOINT}/${slug}`).map((res: any) => res.page);
    }
}