import { Injectable } from "@angular/core";

import { BlogState } from "./../state";
import { Page } from "./page.model";

@Injectable()
export class PageSelector {
    getPages() {
        return (state: BlogState): Page[] => state.page.pages; 
    }

    getCurrent() {
        return (state: BlogState): Page => state.page.current;
    }
}