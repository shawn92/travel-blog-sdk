import { PageAction, PAGE_ACTIONS } from "./page.actions";
import { Page } from "./page.model";
import { PageState } from "./page.state";

export const INITIAL_STATE: PageState = {};

export function pageReducer(state = INITIAL_STATE, action: PageAction) {
    switch (action.type) {
        case PAGE_ACTIONS.loadPagesSuccess: {
            return {
                ...state,
                pages: action.payload as Page[]
            };
        }
        case PAGE_ACTIONS.getPageSuccess: {
            return {
                ...state,
                current: action.payload as Page
            };
        }
        case PAGE_ACTIONS.addPageSuccess: {
            return {
                ...state,
                pages: [...state.pages, action.payload] as Page[]
            };
        }
        case PAGE_ACTIONS.savePageSuccess: {
            const index = state.pages.findIndex((page) => page._id === action.payload._id)
            const pages = [
                ...state.pages.slice(0, index),
                action.payload,
                ...state.pages.slice(index + 1)
            ] as Page[];
            return {
                ...state,
                pages
            };
        }
        case PAGE_ACTIONS.deletePageSuccess: {
            const index = state.pages.findIndex((page) => page._id === action.payload._id)
            const pages = [
                ...state.pages.slice(0, index),
                ...state.pages.slice(index + 1)
            ] as Page[];
            return {
                ...state,
                pages
            };
        }
        case PAGE_ACTIONS.loadPagesFail:
        case PAGE_ACTIONS.getPageFail:
        case PAGE_ACTIONS.addPageFail:
        case PAGE_ACTIONS.savePageFail:
        case PAGE_ACTIONS.deletePageFail: {
            return { 
                ...state,
                error: action.payload 
            };
        }
        case PAGE_ACTIONS.clearCurrentPage: 
            return {
                ...state,
                current: undefined
            };   
        default:
            return state;
    }
};