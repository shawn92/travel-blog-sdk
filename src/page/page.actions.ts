import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";

import { Page } from "./page.model";
import { EditPagePayload } from "./page.state";

export interface PageAction extends Action {
    payload?: any;
}

export const PAGE_ACTIONS = {
    loadPages: "[Page] Load Pages",
    loadPagesSuccess: "[Page] Load Pages Success",
    loadPagesFail: "[Page] Load Pages Fail",

    getPage: "[Page] Get Page",
    getPageSuccess: "[Page] Get Page Success",
    getPageFail: "[Page] Get Page Fail",

    addPage: "[Page] Add Page",
    addPageSuccess: "[Page] Add Page Success",
    addPageFail: "[Page] Add Page Fail",

    savePage: "[Page] Save Page",
    savePageSuccess: "[Page] Save Page Success",
    savePageFail: "[Page] Save Page Fail",

    deletePage: "[Page] Delete Page",
    deletePageSuccess: "[Page] Delete Page Success",
    deletePageFail: "[Page] Delete Page Fail",

    clearCurrentPage: "[Page] Clear Current Page"
};

@Injectable()
export class PageActions {
    loadPages(): PageAction {
        return {
            type: PAGE_ACTIONS.loadPages
        };
    }

    loadPagesSuccess(pages: Page[]): PageAction {
        return {
            type: PAGE_ACTIONS.loadPagesSuccess,
            payload: pages
        };
    }

    loadPagesFail(error: Error): PageAction {
        return {
            type: PAGE_ACTIONS.loadPagesFail,
            payload: error
        };
    }

    getPage(slug: string): PageAction {
        return {
            type: PAGE_ACTIONS.getPage,
            payload: slug
        };
    }

    getPageSuccess(page: Page): PageAction {
        return {
            type: PAGE_ACTIONS.getPageSuccess,
            payload: page
        };
    }

    getPageFail(error: Error): PageAction {
        return {
            type: PAGE_ACTIONS.getPageFail,
            payload: error
        };
    }

    addPage(page: Page): PageAction {
        return {
            type: PAGE_ACTIONS.addPage,
            payload: page
        };
    }

    addPageSuccess(page: Page): PageAction {
        return {
            type: PAGE_ACTIONS.addPageSuccess,
            payload: page
        };
    }

    addPageFail(error: Error): PageAction {
        return {
            type: PAGE_ACTIONS.addPageFail,
            payload: error
        };
    }

    savePage(slug: string, page: Page): PageAction {
        return {
            type: PAGE_ACTIONS.savePage,
            payload: { slug, page } as EditPagePayload
        };
    }

    savePageSuccess(page: Page): PageAction {
        return {
            type: PAGE_ACTIONS.savePageSuccess,
            payload: page
        };
    }

    savePageFail(error: Error): PageAction {
        return {
            type: PAGE_ACTIONS.savePageFail,
            payload: error
        };
    }

    deletePage(page: Page): PageAction {
        return {
            type: PAGE_ACTIONS.deletePage,
            payload: page
        };
    }

    deletePageSuccess(page: Page): PageAction {
        return {
            type: PAGE_ACTIONS.deletePageSuccess,
            payload: page
        };
    }

    deletePageFail(error: Error): PageAction {
        return {
            type: PAGE_ACTIONS.deletePageFail,
            payload: error
        };
    }

    clearCurrentPage(): PageAction {
        return {
            type: PAGE_ACTIONS.clearCurrentPage
        };
    }
}