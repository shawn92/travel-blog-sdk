export interface Page {
    _id: string;
    title: string;
    slug: string;
    content?: string;
    isEnabled?: boolean;
}